#include <iostream>
#include "EasyBMP.h"


using namespace std;

typedef unsigned int ui;

ui const ANCHO = 640;     // ANCHO del mapa
ui const ALTO  = 480;     // ALTO  del mapa
ui const DEPTH = 24;      // Profundidad del color
ui const SEPARACION_ALTO = 10;
ui const SEPARACION_ANCHO = 10;

ui const MARGIN_LEFT = 120;
ui const MARGIN_TOP = 60;

// Suponiendo que todas las imágenes utilizadas para la parcela tienen ese tamanio predefinido
ui const TAMANIO_PARCELA = 50; // 50x50 px;


// Recibe el nombre de una parcela actual.
// Devuelve el estado la imagen representativa del cultivo correspondiente la posicion de la granja.
BMP obtenerImgEstadoParcela(string cultivo){
	BMP parcela;
	if(cultivo.compare("libre") == 0){
		parcela.ReadFromFile("cultivo-libre.bmp");
	}
	else if(cultivo.compare("frutillas") == 0){
		parcela.ReadFromFile("cultivo-frutilla.bmp");
	}
	else if(cultivo.compare("kiwi") == 0){
		parcela.ReadFromFile("cultivo-kiwi.bmp");
	}
	else if(cultivo.compare("lechuga") == 0){
		parcela.ReadFromFile("cultivo-lechuga.bmp");
	}
	else if(cultivo.compare("papa") == 0){
		parcela.ReadFromFile("cultivo-papa.bmp");
	}
	else{
		parcela.ReadFromFile("cultivo-seco.bmp");

	}
	return parcela;
}


int main()
{
    BMP frutillas;
    BMP papas;
    BMP lechuga;
    BMP kiwi;
    BMP parcelaLibre;
    BMP parcelaSeca;
    BMP backround;

    backround.ReadFromFile("granja-background.bmp");
    frutillas.ReadFromFile("cultivo-frutilla.bmp");
    papas.ReadFromFile("cultivo-papa.bmp");
    lechuga.ReadFromFile("cultivo-lechuga.bmp");
    kiwi.ReadFromFile("cultivo-kiwi.bmp");
    parcelaLibre.ReadFromFile("parcela-libre.bmp");
    parcelaSeca.ReadFromFile("parcela-seca.bmp");

	// Se crea un BMP
    BMP mapa;
    // Se setean las medidas
    mapa.SetSize(ANCHO, ALTO);
    // Se setea la profundidad del color (Variedad de colores)
    mapa.SetBitDepth(DEPTH);

    unsigned int i = 0; // col mapa (Ver index mapa manual bmp)
    unsigned int j = 0; //fil mapa (ver index mapa manual bmp)

    //Copia el fondo de pantalla del mapa de alguna otra imagen, en este caso backround -> mapa.
    RangedPixelToPixelCopy(backround, 0, backround.TellWidth() - 1, backround.TellHeight() - 1 , 0,
            mapa, i , j);

    char nombre[] = "imagen-tabla.bmp";

    i = MARGIN_LEFT;
    j= MARGIN_TOP;

    for (unsigned int fil = 0; fil < 5 ; fil++){
		i = 120;
		for (unsigned int col = 0 ; col < 4 ; col++){

			RangedPixelToPixelCopy(parcelaSeca, 0, TAMANIO_PARCELA - 1, TAMANIO_PARCELA - 1, 0,
												mapa, i , j);
			i+= TAMANIO_PARCELA + SEPARACION_ANCHO;
			}
		j+= TAMANIO_PARCELA + SEPARACION_ALTO;
    	}

    cout << "Generando archivo granjeros, por favor espere..." << endl;

    cout << "tamanio de parcela frutilla (50x50) :" << frutillas.TellWidth() << frutillas.TellHeight() << endl;

    // se guarda en el archivo
    mapa.WriteToFile(nombre);

    return 0;
}
